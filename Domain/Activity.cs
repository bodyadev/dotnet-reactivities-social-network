namespace Domain
{

    // This is going do be a table 
    public class Activity
    {
        // And those are the columns in that table
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string City { get; set; }
        public string Venue { get; set; }
    }
    
}