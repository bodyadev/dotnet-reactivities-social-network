using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Activities
{

    public class Edit 
    {
        public class Command : IRequest // Command does not return anything 
        {
            public Activity Activity { get; set; } // Receive this a parameter
        } 

        // Pass the query to handler
        public class Handler : IRequestHandler<Command> 
        {

            // DB CONTEXT to 'talk' to DB once request is received 
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            // Return list of activities 
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
               var activity = await _context.Activities.FindAsync(request.Activity.Id);

               // Update entry 
               activity.Title = request.Activity.Title;
               activity.Date = request.Activity.Date;
               activity.Description = request.Activity.Description;
               activity.Category = request.Activity.Category;
               activity.City = request.Activity.City;
               activity.Venue = request.Activity.Venue;

               // save changes 
               await _context.SaveChangesAsync();

               return Unit.Value;
            }

        }

    }
}