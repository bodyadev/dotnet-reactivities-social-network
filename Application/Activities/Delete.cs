using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Activities
{

    public class Delete 
    {
        public class Command : IRequest // Command does not return anything 
        {
            public Guid Id { get; set; } // Receive this a parameter
        } 

        // Pass the query to handler
        public class Handler : IRequestHandler<Command> 
        {

            // DB CONTEXT to 'talk' to DB once request is received 
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            // Return list of activities 
            public async Task<Unit> Handle(Command request, CancellationToken cancellationToken)
            {
                var activity = await _context.Activities.FindAsync(request.Id);

                _context.Remove(activity);
                
                await _context.SaveChangesAsync();

                // Just letting API controller that task is done
                return Unit.Value;
            }

        }

    }
}