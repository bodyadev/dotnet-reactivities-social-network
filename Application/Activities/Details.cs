

using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Activities
{

    public class Details 
    {
        public class Query : IRequest<Activity> // Body of this class is not empty, as we need to take the parameters ( ID )
        {
            public Guid Id { get; set; }
        } 

        // Pass the query to handler
        public class Handler : IRequestHandler<Query, Activity> 
        {

                // DB CONTEXT to 'talk' to DB once request is received 
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            // Return list of activities 
            public async Task<Activity> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.Activities.FindAsync(request.Id);
            }

        }

    }
}