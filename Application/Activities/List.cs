using Domain;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Persistence;

namespace Application.Activities
{

    public class List
    {

        // Form a query
        public class Query : IRequest<List<Activity>> {} // Return list of Activity objects
        
        // Pass the query to handler
        public class Handler : IRequestHandler<Query, List<Activity>> 
        {

            // DB CONTEXT to 'talk' to DB once request is received 
            private readonly DataContext _context;

            public Handler(DataContext context)
            {
                _context = context;
            }

            // Return list of activities 
            public async Task<List<Activity>> Handle(Query request, CancellationToken cancellationToken)
            {
                return await _context.Activities.ToListAsync();
            }

        }

    }
}